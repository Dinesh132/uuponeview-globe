var attributeService = require('../services/attributeService')

var subjectArea = function(req, res) {
    attributeService.getSubjectArea(function(err, results) { 
      console.log('subject Area Controller')
      if (err)
        res.send(err);
        console.log('res', results);
      res.json(results);
    });
  };

  var attributeStats = function(req, res) {
    attributeService.getAttributeStats(function(err, results) { 
      console.log('attribute Stats Controller')
      if (err)
        res.send(err);
        console.log('res', results);
      res.json(results);
    });
  };
  
exports.subjectArea = subjectArea
exports.attributeStats = attributeStats