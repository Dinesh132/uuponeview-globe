var uptimeService = require('../services/uptimeService')

var checkServiceUptime = function(req, res) {
    uptimeService.checkService(req.params.serviceName,function(err, results) { 
      console.log('Service Uptime Controller')
      if (err)
        res.send(err);
        console.log('res', results);
      res.json(results);
    });
  };

  exports.checkServiceUptime = checkServiceUptime

  