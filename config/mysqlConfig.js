var mysql = require('mysql');

var pool  = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : 'mysql123',
    database : 'oneview'
});

exports.pool = pool;
/*
let attributeRefreshQuery = "select m.attribute_name,a.subject_area,a.last_refresh_date from uup_attribute_refresh_info a JOIN uup_attribute_master m ON a.subject_area=m.subject_area"
let uupJobsQuery  = "select job_name,job_end_timestamp-job_start_timestamp as job_run_time,job_run_date from uup_job_info order by job_name,job_run_date asc"
let subjectAreaQuery = "select count(attribute_name),subject_area from oneview_attribute_master group by subject_area"
	
const getAttributesRefresh = (request, response) => {
	  pool.query(attributeRefreshQuery, (error, results) => {
	    if (error) {
        console.error();
        console.log(error);
        
	      throw error
	    }
	    else {
	    	response.status(200).json(results.rows)
	    }
	    
	  })
	}

const getJobMetrics = (request, response) => {
	  pool.query(uupJobsQuery, (error, results) => {
	    if (error) {
        console.error();
        console.log(error);
	      throw error
	    }
	    else {
	    	console.log(results.rows)
	    	response.status(200).json(results.rows)
	    }
	    
	  })
	}

const getSubjectArea = (request, response) => {
	  pool.query(subjectAreaQuery, (error, results) => {
	    if (error) {
        throw error
	    }
	    else {
	    	console.log("here"+results)
	    	response.status(200).json(results)
	    }
	    
	  })
	}

const updateAdvisory = (request, response) => {
	  const id = parseInt(request.params.id)
	  const { priority} = request.body

	  pool.query(
	    '',
	    [id,priority],
	    (error, results) => {
	      if (error) {
	        throw error
	      }
	      response.status(200).send(`User modified with ID: ${id}`)
	    }
	  )
	}

module.exports = {
		getAttributesRefresh,
		getJobMetrics,
		updateAdvisory,
		getSubjectArea,
    }
    */