module.exports = function(app) {

    var attrController = require('../controller/attributeController');
    var uptimeController = require('../controller/uptimeController')
    var jobsController = require('../controller/jobsController')
    

    app.route('/oneview')
        .get(function(request,response){
            response.json({ info: 'Enterprise Data Office Globe Telecom' })
        })

    app.route('/oneview/subjectArea')
        .get(attrController.subjectArea);

    app.route('/oneview/attributes')
        .get(attrController.attributeStats)

    app.route('/oneview/checkServiceUptime/:serviceName')
        .get(uptimeController.checkServiceUptime)

    app.route('/oneview/jobsMetrics')
        .get(jobsController.jobsMetrics)
};