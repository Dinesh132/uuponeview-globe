
const express = require('express')
const bodyParser = require('body-parser')
const app = express()

var routes = require('./routes/oneviewRoutes'); //importing route

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)
routes(app);

module.exports = app
