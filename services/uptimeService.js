var urlExists = require('url-exists');

var serviceLookupTable = {
"audience": "https://www.google.com",
"profiles": "https://www.fakeurl.notreal"
};

var checkService = function (serviceName, result) {
	console.log(serviceName +" "+serviceLookupTable[serviceName])
	urlExists(serviceLookupTable[serviceName], function(err, exists) {
		result(null,exists)
	});
}
 
exports.checkService = checkService