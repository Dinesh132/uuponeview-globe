
var mySqlConn = require('../config/mysqlConfig').pool;

var getSubjectArea = function (result) {
	var getSubAreaQry = "select count(attribute_name) as count,subject_area from oneview_attribute_master group by subject_area";
	mySqlConn.query(getSubAreaQry, function (err, rows) {
			if(err) {
				console.log("error: ", err);
				result(null, err);
			}
			else{
			  console.log('results : ', rows);  
			 		result(null, rows);
			}
		});   
};

var getAttributeStats = function (result) {
	var attrStatsQry = "select m.attribute_name,a.subject_area,a.last_refresh_date from oneview_attribute_refresh_info a JOIN oneview_attribute_master m ON a.subject_area=m.subject_area";
	mySqlConn.query(attrStatsQry, function (err, rows) {
			if(err) {
				console.log("error: ", err);
				result(null, err);
			}
			else{
			  console.log('results : ', rows);  
			 		result(null, rows);
			}
		});   
};

exports.getSubjectArea = getSubjectArea
exports.getAttributeStats = getAttributeStats