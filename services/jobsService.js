var mySqlConn = require('../config/mysqlConfig').pool;

var getJobsMetrics = function (result) {
	var getJobsMetricsQry = "select job_name,job_end_timestamp-job_start_timestamp as job_run_time,job_run_date from oneview_job_metrics order by job_name,job_run_date asc";
	mySqlConn.query(getJobsMetricsQry, function (err, rows) {
			if(err) {
				console.log("error: ", err);
				result(null, err);
			}
			else{
			  console.log('results : ', rows);  
			 		result(null, rows);
			}
		});   
};

exports.getJobsMetrics = getJobsMetrics