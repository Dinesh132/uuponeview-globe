
const port = 3000

var app = require('./oneview')

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  })