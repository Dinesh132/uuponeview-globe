/*global $, document, LINECHARTEXMPLE*/

var dpaArr = []
var redArr = []
var dynArr = []
var profArr = []
var serverUrl = "http://localhost"
var serverPort = "3000"

function populateLineChart(jobsJson){
	$.each(jobsJson,function(i,obj){
		//alert(obj.job_name)
		if(obj.job_name == "DPA Extract"){
			dpaArr.push(obj.job_run_time.minutes)
		}
		if(obj.job_name == "Dynamo Population"){
			dynArr.push(obj.job_run_time.minutes)
		}
		if(obj.job_name == "Redshift Population"){
			redArr.push(obj.job_run_time.minutes)
		}
		if(obj.job_name == "Profile Calc"){
			profArr.push(obj.job_run_time.minutes)
		}
		
	});
	//alert(dpaArr)
}

function drawLineChart(){
    'use strict';
    var brandPrimary = 'rgba(51, 179, 90, 1)';
    var LINECHARTEXMPLE   = $('#lineChartExample');
    var lineChartExample = new Chart(LINECHARTEXMPLE, {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "DPA Extract",
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: "rgba(51, 179, 90, 0.38)",
                    borderColor: brandPrimary,
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: brandPrimary,
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: brandPrimary,
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: dpaArr,
                    spanGaps: false
                },
                {
                    label: "Redshift",
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: redArr,
                    spanGaps: false
                },
                {
                    label: "Dynamo",
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: dynArr,
                    spanGaps: false
                },
                {
                    label: "Profile",
                    fill: false,
                    lineTension: 0.3,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    borderWidth: 1,
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: profArr,
                    spanGaps: false
                }
            ]
        }
    });
}

$(document).ready(function () {
	//alert('here')

	 $.ajax({
         contentType: "application/json; charset=utf-8",
         url: serverUrl+':'+serverPort+'/jobs/',
         dataType: 'json',
         async: true,         
         success: function (data) {
            populateLineChart(data)
            drawLineChart()
            
         },
         error: function (result) {
         }
	 })
});