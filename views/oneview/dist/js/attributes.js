var serverUrl = "http://localhost"
var serverPort = "3000"
	
$(document).ready(function() { 
    $.ajax({
         contentType: "application/json; charset=utf-8",
         url: serverUrl+':'+serverPort+'/attributesview/',
         dataType: 'json',
         async: true,         
         success: function (data) {
            $('#attributestable').bootstrapTable({
                data: data
            });
         },
         error: function (result) {
}
})
});