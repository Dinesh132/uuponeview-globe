/*global $, document, Chart, LINECHART, data, options, window*/

var subjAreaLabels = []
var subjAreaAttrCount = []
var serverUrl = "http://localhost"
var serverPort = "3000"

function populateSubjectArea(subjAreaJson){
	$.each(subjAreaJson,function(i,obj){
		subjAreaLabels.push(obj.subject_area)
		subjAreaAttrCount.push(obj.count)
	});
}
function checkService(portal){
	$.ajax({
        contentType: "application/json; charset=utf-8",
        url: serverUrl+':'+serverPort+'/checkServiceStatus/'+portal,
        dataType: 'json',
        async: false,         
        success: function (data) {
           if(data){
        	   $("#"+portal).removeClass('card bg-warning mb-3').addClass('card bg-success mb-3')  
           }
           else {
        	   $("#"+portal).removeClass('card bg-success mb-3').addClass('card bg-warning mb-3')
           } 
        },
        	error: function (result) {
        }
	 })
}

function drawPieChart(){
	 'use strict';
	    // Main Template Color
	    var brandPrimary = '#33b35a';
	    var PIECHART = $('#subjArea');
	    var myPieChart = new Chart(PIECHART, {
	        type: 'doughnut',
	        data: {
	            labels: subjAreaLabels,
	            datasets: [
	                {
	                    data: subjAreaAttrCount,
	                    borderWidth: [1, 1, 1],
	                    backgroundColor: [
	                        brandPrimary,
	                        "rgba(75,192,192,1)",
	                        "#FFCE56","#8cb333","#b3334c","#b3338c"
	                    ],
	                    hoverBackgroundColor: [
	                        brandPrimary,
	                        "rgba(75,192,192,1)",
	                        "#FFCE56","#8cb333","#b3334c","#b3338c"
	                    ]
	                }]
	        }
	    });	
}

$(document).ready(function () {
	 $.ajax({
         contentType: "application/json; charset=utf-8",
         url: serverUrl+':'+serverPort+'/subjectarea/',
         dataType: 'json',
         async: true,         
         success: function (data) {        	
            populateSubjectArea(data)
            drawPieChart()
            
         },
         	error: function (result) {
         }
	 }) 
	 checkService('audience');
	 checkService('profiles');
});
	 

   

